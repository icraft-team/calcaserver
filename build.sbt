import java.text.SimpleDateFormat
import java.util.Calendar

def currentDateAndTime : String = {
  val format = new SimpleDateFormat("d.M.y_H:m")
  format.format(Calendar.getInstance().getTime)
}

organization  := "com.example"

assemblyJarName in assembly := "calcaserver_" + currentDateAndTime + ".jar"

target in assembly := file("ciBuilds")

name := "CalcaServer"

version := "0.1"

scalaVersion := "2.11.8"

val akkaVersion  = "2.3.15"
val sprayVersion  = "1.3.3"

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8", "-Xmax-classfile-name", "242")

resolvers += "spray repo" at "http://repo.spray.io"

def spray = Seq(
  "io.spray"            %%  "spray-can"     % sprayVersion,
  "io.spray"            %%  "spray-routing" % sprayVersion,
  "io.spray"            %%  "spray-json"    % sprayVersion,
  "io.spray"            %%  "spray-testkit" % sprayVersion  % "test",
  "com.typesafe.akka"   %%  "akka-actor"    % akkaVersion,
  "com.typesafe.akka"   %%  "akka-testkit"  % akkaVersion   % "test",
  "org.specs2"          %%  "specs2-core"   % "2.3.11" % "test"
)

def db = Seq(
  "c3p0"        % "c3p0"                 % "0.9.1.2",
  "mysql"       % "mysql-connector-java" % "5.1.10",
  "org.squeryl" %% "squeryl"             % "0.9.5-7"
)

def gson = Seq(
  "com.google.code.gson" % "gson" % "2.3"
)

libraryDependencies ++= (spray ++ db ++ gson)

Revolver.settings

retrieveManaged := true
//assemblyMergeStrategy in assembly := {
//  case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
//}

//assemblyMergeStrategy in assembly := {
//  case n if n.startsWith("META-INF/eclipse.inf") => MergeStrategy.discard
//  case n if n.startsWith("META-INF/ECLIPSEF.RSA") => MergeStrategy.discard
//  case n if n.startsWith("META-INF/ECLIPSE_.RSA") => MergeStrategy.discard
//  case n if n.startsWith("META-INF/ECLIPSEF.SF") => MergeStrategy.discard
//  case n if n.startsWith("META-INF/ECLIPSE_.SF") => MergeStrategy.discard
//  case n if n.startsWith("META-INF/MANIFEST.MF") => MergeStrategy.discard
//  case n if n.startsWith("META-INF/NOTICE.txt") => MergeStrategy.discard
//  case n if n.startsWith("META-INF/NOTICE") => MergeStrategy.discard
//  case n if n.startsWith("META-INF/LICENSE.txt") => MergeStrategy.discard
//  case n if n.startsWith("META-INF/LICENSE") => MergeStrategy.discard
//  case n if n.startsWith("rootdoc.txt") => MergeStrategy.discard
//  case n if n.startsWith("readme.html") => MergeStrategy.discard
//  case n if n.startsWith("readme.txt") => MergeStrategy.discard
//  case n if n.startsWith("library.properties") => MergeStrategy.discard
//  case n if n.startsWith("license.html") => MergeStrategy.discard
//  case n if n.startsWith("about.html") => MergeStrategy.discard
//  case n if n.startsWith("reference.conf") => MergeStrategy.discard
//  case PathList("reference.conf") => MergeStrategy.concat
//  case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
//  case _ => MergeStrategy.deduplicate
//}


