package db

import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.{Calendar, Date, SimpleTimeZone, TimeZone}

class TimestampRich(val timestamp: Timestamp) {
  def before(period: Long) = after(-period)

  def after(periodMs: Long) = {
    new java.sql.Timestamp(timestamp.getTime + periodMs)
  }
}

object Time {
  val timeFormat = "yyyy-MM-dd HH:mm:ss Z"
  val SEC = 1000L
  val MIN = SEC * 60L
  val HOUR = MIN * 60L
  val ONE_DAY = 24L * HOUR

  def timestampToTimestampRich(timestamp: Timestamp) : TimestampRich = new TimestampRich(timestamp)

  val Beginning = new Timestamp(0)

  def now = {
    val date = new java.util.Date()
    new Timestamp(date.getTime)
  }

  def before(period: Long) = after(-period)

  def after(period: Long) = {
    val now = new java.util.Date()
    new java.sql.Timestamp(now.getTime + period)
  }

  def nowString : String = {
    val date = new Date()
    val format = new SimpleDateFormat(timeFormat)
    format.setTimeZone(TimeZone.getTimeZone("GMT"));
    format.format(date)
  }

  def startOfToday: Long = {
    val cal = Calendar.getInstance()
    cal.set(Calendar.HOUR_OF_DAY, 0)
    cal.set(Calendar.MINUTE, 0)
    cal.set(Calendar.SECOND, 0)
    cal.set(Calendar.MILLISECOND, 0)
    val seconds = cal.getTimeInMillis / 1000
    seconds
  }

  def stringFromEpoch(seconds: Long): String = {
    val date = new Date(seconds * 1000)
    val format = new SimpleDateFormat(timeFormat)
    format.setTimeZone(TimeZone.getTimeZone("GMT"));
    format.format(date)
  }
}
