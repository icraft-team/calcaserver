package db

import java.io.FileReader
import java.sql.SQLException

import scala.runtime.NonLocalReturnControl

import com.google.gson.Gson
import com.mchange.v2.c3p0.ComboPooledDataSource
import org.squeryl.{Session, SessionFactory}
import org.squeryl.internals.{DatabaseAdapter, Utils}

/**
  * Created by Valentin on 13/07/16.
  */

case class Configuration(adapterClass: String,
                         driverClass: String,
                         dbURL: String,
                         user: String,
                         password: String)

object DBSession {
  val pool = new ComboPooledDataSource
  var adapterClass: Class[_] = null

  def configure(fileName: String) {
    val reader = new FileReader(fileName)
    val gson = new Gson
    val configuration = gson.fromJson(reader, classOf[Configuration])

    pool.setDriverClass(configuration.driverClass)
    pool.setJdbcUrl(configuration.dbURL)
    pool.setUser(configuration.user)
    pool.setPassword(configuration.password)

    adapterClass = Class.forName(configuration.adapterClass)

    SessionFactory.concreteFactory = Option(create _)
  }

  def create = {
    val adapter = adapterClass.newInstance().asInstanceOf[DatabaseAdapter]
    val session = Session.create(pool.getConnection, adapter)
    session
  }
}

trait DBSession {
  def transaction[A](a: =>A): A = {
    val result = if (!Session.hasCurrentSession) {
      _executeTransactionWithin(SessionFactory.newSession, a _)
    } else {
      val s = Session.currentSession
      val res =
        try {
          s.unbindFromCurrentThread
          _executeTransactionWithin(SessionFactory.newSession, a _)
        }
        finally {
          s.bindToCurrentThread
        }
      res
    }
    result
  }

  private def _executeTransactionWithin[A](s: Session, a: ()=>A) = {
    val c = s.connection
    val originalAutoCommit = c.getAutoCommit
    if(originalAutoCommit) c.setAutoCommit(false)

    var txOk = false
    try {
      val res = _using(s, a)
      txOk = true
      res
    }
    catch {
      case e:NonLocalReturnControl[_] =>
      {
        txOk = true
        throw e
      }
    }

    finally {
      try {
        if(txOk)
          c.commit
        else
          c.rollback
        if(originalAutoCommit != c.getAutoCommit)
          c.setAutoCommit(originalAutoCommit)
      }
      catch {
        case e:SQLException => {
          Utils.close(c)
          if(txOk) throw e // if an exception occured b4 the commit/rollback we don't want to obscure the original exception
        }
      }
      try{c.close}
      catch {
        case e:SQLException => {
          if(txOk) throw e // if an exception occured b4 the close we don't want to obscure the original exception
        }
      }
    }
  }

  private def _using[A](session: Session, a: ()=>A): A = {
    val s = Session.currentSessionOption
    try {
      if(s != None) s.get.unbindFromCurrentThread
      try {
        session.bindToCurrentThread
        val r = a()
        r
      }
      finally {
        session.unbindFromCurrentThread
        session.cleanup
      }
    }
    finally {
      if(s != None) s.get.bindToCurrentThread
    }
  }
}

class BasicSession extends DBSession