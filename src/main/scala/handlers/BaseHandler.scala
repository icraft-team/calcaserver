package handlers

import com.google.gson.{ExclusionStrategy, FieldAttributes, Gson, GsonBuilder}
import db.DBSession
import spray.http.HttpResponse

/**
  * Created by Valentin on 26/01/17.
  */


trait JsonProcessor {
  val gson = new Gson()
}

abstract class BaseHandler extends AbstractHandler with JsonProcessor with DBSession {
  def process : (HttpResponse)
  def checkAuth(authToken : String) : Boolean = true

  def auth[A](token: String, a: =>A): A = {
    a
  }
}

trait Measurable {
  var startTime: Long = 0
  var endTime: Long = 0
  def startMeasure() {
    startTime = System.nanoTime()
  }
  def stopMeasure() {
    endTime = System.nanoTime()
    println("Elapsed time: " + (endTime - startTime) + " nanoseconds")
  }
}
