package handlers

import helpers.DishCache
import schema.{Calca, Dish}
import spray.http.{HttpResponse, StatusCodes}
import org.squeryl.PrimitiveTypeMode._

/**
  * Created by Valentin on 06/05/2017.
  */
class CreateDishHandler(val json : String = "", authToken : String) extends BaseHandler {
  class CreateDishRequest() {
    var name : String = ""
    var calsPer : Int = 0
    var singled : Boolean = false
  }

  def process : (HttpResponse) = {
    val request = gson.fromJson(json, classOf[CreateDishRequest])
    transaction{
      Calca.dishes.where(d => d.name === request.name).headOption match {
        case Some(d) => HttpResponse(StatusCodes.Conflict, "dish with this name already exists")
        case None => {
          val dish = new Dish(request.name, request.calsPer, request.singled)
          Calca.dishes.insert(dish)
          DishCache.addDish(dish)
          HttpResponse(StatusCodes.OK, gson.toJson(dish))
        }
      }
    }
  }
}
