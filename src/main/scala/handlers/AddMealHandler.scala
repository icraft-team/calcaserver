package handlers

import db.Time
import helpers.Utils
import helpers.auth.SimpleAuthManager
import model.Profile
import schema.{Calca, Dish, Meal}
import spray.http.{HttpResponse, StatusCodes}

/**
  * Created by Valentin on 01/05/2017.
  */
class AddMealHandler(val json : String = "", authToken : String) extends BaseHandler {
  class AddMealRequest () {
    var selectedDish : Dish = null
    var numberOfGramsOrServings : Float = 0
    var secondsBeforeThisMealSince1970 : Long = 0
  }

  def process : (HttpResponse) = {
    val request = gson.fromJson(json, classOf[AddMealRequest])

    val meal = new Meal(request.selectedDish)
    meal.configure()
    meal.userID = SimpleAuthManager.getUserIdByToken(authToken)

    if (request.secondsBeforeThisMealSince1970 == 0) meal.time = Time.now.getTime / 1000
    else meal.time = request.secondsBeforeThisMealSince1970

    meal.numberOfGramsOrServings = request.numberOfGramsOrServings.toInt
    meal.totalCals = if (request.selectedDish.singled) {
      (request.selectedDish.calsPer * request.numberOfGramsOrServings).toInt
    } else {
      (request.numberOfGramsOrServings / 100 * request.selectedDish.calsPer).toInt
    }

    transaction {
      Calca.meals.insert(meal)
      val profile = Profile.getWithToken(authToken)
      HttpResponse(StatusCodes.OK, gson.toJson(profile))
    }
  }
}
