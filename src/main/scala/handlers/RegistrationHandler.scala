package handlers

import helpers.Utils
import schema.{Calca, User}
import spray.http.{HttpResponse, StatusCodes}
import org.squeryl.PrimitiveTypeMode._

/**
  * Created by Valentin on 08/02/17.
  */

class RegistrationHandler(json : String) extends BaseHandler {
  class RegistrationRequest {
    val login : String = ""
    val pass : String = ""
    val email : String = ""
    val timezoneOffset : Long = 0L
  }

  def process : (HttpResponse) = {
    val request = gson.fromJson(json, classOf[RegistrationRequest])

    transaction {
      Calca.users.where(u => u.login === request.login).headOption match {
        case Some(u) => return HttpResponse(StatusCodes.Conflict, "this login already exist")
        case None =>
      }

      Calca.users.where(u => u.email === request.email).headOption match {
        case Some(u) => return HttpResponse(StatusCodes.Gone, "this email already exist")
        case None =>
      }

      val passHash = Utils.md5(request.pass)
      val user = new User(request.login, passHash, request.email)
      user.timezoneOffset = request.timezoneOffset

      Calca.users.insert(user)
    }

    new AuthHandler().login(request.login, request.pass)
  }
}
