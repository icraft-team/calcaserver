package handlers

import db.Time
import spray.http.{HttpResponse, StatusCodes}

/**
 * Created by Valentin on 04/12/14.
 */
class PingHandler extends AbstractHandler {
  def process = {
    print(Time.startOfToday)
    HttpResponse(StatusCodes.Unauthorized, "Naaah, boy!!")
  }
}
