package handlers
import model.Profile
import schema.Calca
import spray.http.{HttpResponse, StatusCodes}
import org.squeryl.PrimitiveTypeMode._

/**
  * Created by Valentin on 01/05/2017.
  */

class MealListHandler(val json : String = "", authToken : String) extends BaseHandler {
  class MealListRequest () {
    var startDateInterval : Long = 0
    var endDateInterval : Long = 0
  }

  def process : (HttpResponse) = {
    val request = gson.fromJson(json, classOf[MealListRequest])

    val start = request.startDateInterval
    val end = request.endDateInterval

    transaction {
      val profile = Profile.getWithToken(authToken)

      val meals = Calca.meals.where(m =>
        m.time.gte(start) and
        m.time.lte(end) and
        m.userID === profile.user.id
      ).toArray

      HttpResponse(StatusCodes.OK, gson.toJson(meals))
    }
  }
}

