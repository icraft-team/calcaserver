package handlers

import helpers.auth.SimpleAuthManager
import schema.{Calca, Meal, PerformedExercise}
import spray.http.{HttpResponse, StatusCodes}
import org.squeryl.PrimitiveTypeMode._

/**
  * Created by Valentin on 02/03/2018.
  */

class UserActivitiesHandler(val json : String = "", authToken : String) extends BaseHandler {
  class ActivitiesRequest () {
    var startDateInterval : Long = 0
    var endDateInterval : Long = 0
  }

  class ActivitiesResponse (val meals : Array[Meal], val exercises : Array[PerformedExercise])

  def process : (HttpResponse) = {
    val request = gson.fromJson(json, classOf[ActivitiesRequest])

    val start = request.startDateInterval
    val end = request.endDateInterval

    transaction {
      val userID = SimpleAuthManager.getUserIdByToken(authToken)

      val meals = Calca.meals.where(m =>
        m.time.gte(start) and
          m.time.lte(end) and
          m.userID === userID
      ).toArray

      val exercises = Calca.performedExercises.where(e =>
        e.time.gte(start) and
          e.time.lte(end) and
          e.userID === userID
      ).toArray

      val response = new ActivitiesResponse(meals, exercises)

      HttpResponse(StatusCodes.OK, gson.toJson(response))
    }
  }
}
