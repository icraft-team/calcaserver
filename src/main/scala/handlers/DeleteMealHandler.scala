package handlers

import helpers.auth.SimpleAuthManager
import model.Profile
import schema.Calca
import spray.http.{HttpResponse, StatusCodes}
import org.squeryl.PrimitiveTypeMode._


/**
  * Created by Valentin on 25/05/2017.
  */
class DeleteMealHandler(mealID : Long, authToken : String) extends BaseHandler {
  def process : (HttpResponse) = {
    val userID = SimpleAuthManager.getUserIdByToken(authToken)
    transaction {
      Calca.meals.deleteWhere(m => m.id === mealID and m.userID === userID)
      HttpResponse(StatusCodes.OK, gson.toJson(Profile.getWithToken(authToken)))
    }
  }
}
