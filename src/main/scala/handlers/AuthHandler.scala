package handlers

import com.google.gson.Gson
import schema.Calca
import helpers.Utils
import helpers.auth.SimpleAuthManager
import model.Profile
import org.squeryl.PrimitiveTypeMode._
import spray.http._
import spray.http.StatusCodes
import spray.http.ContentTypes

/**
  * Created by Valentin on 26/01/17.
  */

class AuthRequest(val login: String, val pass: String)
class RestoreAuthRequest(val authToken: String)

class AuthHandler(val json : String = "") extends BaseHandler with Measurable {
  def process : (HttpResponse) = {
    val request = gson.fromJson(json, classOf[AuthRequest])
    login(request.login, request.pass)
  }

  def login(login : String, pass : String) : (HttpResponse) = {
    transaction {
      startMeasure()
      Calca.users.where(u =>
        u.passHash === Utils.md5(pass) and u.login === login).headOption match {
        case Some(u) =>
          val authToken = java.util.UUID.randomUUID.toString
          SimpleAuthManager.addToken(authToken, u.id)
          val header = HttpHeaders.`Set-Cookie`(HttpCookie("authToken", authToken))

          stopMeasure()
          HttpResponse(StatusCodes.OK, gson.toJson(new Profile(u, authToken))).withHeaders(header)
        case None => HttpResponse(StatusCodes.Unauthorized, "wrong login or pass")
      }
    }
  }

  def restoreAuth(json : String) : (HttpResponse) = {
    val request = gson.fromJson(json, classOf[RestoreAuthRequest])
    if (SimpleAuthManager.check(request.authToken)) {
      transaction {
        val userID = SimpleAuthManager.getUserIdByToken(request.authToken)
        Calca.users.where(u => u.id === userID).headOption match {
          case Some(u) =>
            SimpleAuthManager.removeToken(request.authToken)
            val newToken = java.util.UUID.randomUUID.toString
            SimpleAuthManager.addToken(newToken, u.id)
            val header = HttpHeaders.`Set-Cookie`(HttpCookie("authToken", newToken))
            HttpResponse(StatusCodes.OK, gson.toJson(new Profile(u, newToken))).withHeaders(header)
          case None => HttpResponse(StatusCodes.Forbidden, "user with this token not found")
        }
      }
    } else {
      HttpResponse(StatusCodes.Unauthorized, "token expired")
    }
  }
}
