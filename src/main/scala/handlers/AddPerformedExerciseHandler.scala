package handlers

import helpers.auth.SimpleAuthManager
import model.Profile
import schema.{Calca, Exercise, PerformedExercise}
import spray.http.{HttpResponse, StatusCodes}

/**
  * Created by Valentin on 02/03/2018.
  */
class AddPerformedExerciseHandler(val json : String = "", authToken : String) extends BaseHandler {
  class AddPerformedExerciseRequest {
    var selectedExercise : Exercise = null
    var minutes : Int = 0
    var epochDateSeconds : Long = 0 // seconds
  }

  def process : (HttpResponse) = {
    val request = gson.fromJson(json, classOf[AddPerformedExerciseRequest])

    transaction {
      val profile = Profile.getWithToken(authToken)

      val performedExercise = new PerformedExercise()
      performedExercise.name = request.selectedExercise.name
      performedExercise.exerciseID = request.selectedExercise.id
      performedExercise.userID = profile.user.id
      performedExercise.time = request.epochDateSeconds
      performedExercise.caloriesBurnt = request.selectedExercise.caloriesBurnt(profile.user.weightInKilos, request.minutes)

      Calca.performedExercises.insert(performedExercise)
    }

    HttpResponse(StatusCodes.OK, gson.toJson(""))
  }
}
