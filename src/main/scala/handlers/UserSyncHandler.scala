package handlers

import java.text.SimpleDateFormat
import java.util.{Date, SimpleTimeZone}

import db.Time
import model.Profile
import schema.{Calca, User}
import spray.http.{HttpResponse, StatusCodes}
import org.squeryl.PrimitiveTypeMode._

/**
  * Created by Valentin on 15/01/2018.
  */
class UserSyncHandler(val json : String = "", authToken : String) extends BaseHandler {
  def process : (HttpResponse) = {
    val user = gson.fromJson(json, classOf[User])


    transaction {
      Calca.users.where(u => u.id === user.id).headOption match {
        case Some(u) => {
          print("user start of day: " + (Time.startOfToday + u.timezoneOffset))
          u.caloriesPerDay = user.caloriesPerDay
          u.phone = user.phone
          u.timezoneOffset = user.timezoneOffset
          u.weight = user.weight
          Calca.users.update(u)
          HttpResponse(StatusCodes.OK, gson.toJson(Profile.getWithToken(authToken)))
        }
        case None => HttpResponse(StatusCodes.NotFound, "")
      }
    }
  }
}
