package handlers

import helpers.auth.SimpleAuthManager
import model.Profile
import schema.Calca
import spray.http.{HttpResponse, StatusCodes}
import org.squeryl.PrimitiveTypeMode._

/**
  * Created by Valentin on 06/03/2018.
  */
class DeleteExerciseHandler(exerciseID: Long, authToken: String) extends BaseHandler {
  def process : (HttpResponse) = {
    val userID = SimpleAuthManager.getUserIdByToken(authToken)
    transaction {
      Calca.performedExercises.deleteWhere(e => e.id === exerciseID and e.userID === userID)
      HttpResponse(StatusCodes.OK, gson.toJson(Profile.getWithToken(authToken)))
    }
  }
}
