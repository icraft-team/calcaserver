package handlers

/**
  * Created by Valentin on 01/05/2017.
  */
import helpers.DishCache
import spray.http._

class DishAutocompleteHandler(searchString: String) extends BaseHandler {
  def process : (HttpResponse) = {
    val decodedString = java.net.URLDecoder.decode(searchString, "UTF-8");
    HttpResponse(StatusCodes.OK, gson.toJson(DishCache.search(decodedString).toArray))
  }
}
