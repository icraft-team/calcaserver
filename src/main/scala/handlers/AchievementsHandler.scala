package handlers

import helpers.AchievementStore
import helpers.auth.SimpleAuthManager
import schema.{Calca, Meal, PerformedExercise, Reward, User}
import spray.http.{HttpResponse, StatusCodes}
import org.squeryl.PrimitiveTypeMode._

/**
  * Created by Valentin on 22/03/2018.
  */
class UserData(val user: User, val meals: Array[Meal], val exercises: Array[PerformedExercise], val rewards: Array[Reward])

class AchievementsHandler(authToken : String) extends BaseHandler with Measurable {
  def process : (HttpResponse) = {
    startMeasure()
    transaction {

      val userID = SimpleAuthManager.getUserIdByToken(authToken)
      val meals = Calca.meals.where(m =>
        m.userID === userID
      ).toArray

      val exercises = Calca.performedExercises.where(e =>
        e.userID === userID
      ).toArray

      val user = Calca.users.where(u => u.id === userID).head
      val rewards = Calca.rewards.where(u => u.id === userID).toArray
      val data = new UserData(user, meals, exercises, rewards)

      val achievementStore = new AchievementStore(data)

      stopMeasure()
      HttpResponse(StatusCodes.OK, gson.toJson(achievementStore))
    }
  }
}
