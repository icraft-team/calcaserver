package handlers

import spray.http.{HttpResponse, StatusCodes}

/**
 * Created by Valentin on 04/12/14.
 */
class TestHandler (val number : Int) extends AbstractHandler {
  def process = {
    HttpResponse(StatusCodes.Unauthorized, "Naaah, boy!!")
  }
}
