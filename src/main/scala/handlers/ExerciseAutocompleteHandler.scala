package handlers

import helpers.ExerciseCache
import spray.http.{HttpResponse, StatusCodes}

/**
  * Created by Valentin on 14/02/2018.
  */

class ExerciseAutocompleteHandler(searchString: String) extends BaseHandler {
  def process : (HttpResponse) = {
    val decodedString = java.net.URLDecoder.decode(searchString, "UTF-8")
    HttpResponse(StatusCodes.OK, gson.toJson(ExerciseCache.search(decodedString).toArray))
  }
}
