package helpers.auth

import java.sql.Timestamp
import db.Time

object SimpleAuthManager extends AuthManager {

  protected var tokens: Set[AuthEntry] = Set.empty

  override def check(authToken : String) = {
    get(authToken).isDefined
  }

  override def addToken(authToken : String, userId : Long) {
    val entry = AuthEntry(authToken, userId, Time.now)
    tokens = tokens.filter(t => t.userId != userId) + entry
  }

  override def removeToken(authToken: String): Unit = {
    tokens = tokens.filter(t => t.token != authToken)
  }

  override def getUserIdByToken(authToken : String): Long = {
    tokens.find(_.token == authToken)
      .map(_.userId)
      .getOrElse(-1)
  }

  protected def get(authToken : String) = {
    tokens.find(_.token == authToken)
  }
}

case class AuthEntry(token: String, userId: Long, timestamp: Timestamp)
