package helpers

/**
  * Created by Valentin on 01/05/2017.
  */
import db.DBSession
import schema.{Calca, Dish}
import org.squeryl.PrimitiveTypeMode._

import scala.collection.mutable.ListBuffer

object DishCache extends DBSession {
  protected var dishes: ListBuffer[Dish] = ListBuffer.empty
  protected val syncTime : Long = 60

  def start() {
    sync()
    val t = new java.util.Timer()
    val task = new java.util.TimerTask { def run() = sync() }
    t.schedule(task, syncTime * 1000, syncTime * 1000L)
  }

  protected def sync() {
    transaction {
      dishes = Calca.dishes.to[ListBuffer]
    }
  }

  def search(string: String) : ListBuffer[Dish] = {
    dishes.filter(m => m.name.toLowerCase.contains(string.toLowerCase))
  }

  def addDish(dish: Dish) {
    dishes += dish
  }
}
