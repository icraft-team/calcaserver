package helpers

import handlers.UserData
import model.Achievement
import model.achievements._

/**
  * Created by Valentin on 22/03/2018.
  */
class AchievementStore(@transient val data: UserData) {
  var list: Array[Achievement] = Array(
    new BurnCalories10000Achievement(),
    new BurnCaloriesAchievementDaily()
  )

  for(achievement <- list) {
    achievement.calculateProgress(data.user, data.meals, data.exercises)
    if (achievement.progress >= 1) achievement.checkReward(data.user, data.rewards)
  }
}
