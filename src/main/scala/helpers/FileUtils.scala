package helpers

import java.io._
import Utils._




import scala.util.Try

/**
  * Created by Alpex on 03.08.2016.
  */
object FileUtils {

  def writeFile(path: String, bytes: Array[Byte]): Try[Array[Byte]] = {
    var fosOpt: Option[OutputStream] = None
    Try {
      val fos = new FileOutputStream(new File(path))
      fosOpt = Some(fos)
      fos.write(bytes)
      bytes
    } eventually {
      fosOpt.foreach(_.close())
    }
  }

  def readFile(path: String): Try[Array[Byte]] = {
    val file = new File(path)
    var finOpt: Option[InputStream] = None
    Try {
      val fin = new FileInputStream(file)
      finOpt = Some(fin)
      val byteArray = new Array[Byte](file.length().toInt)
      fin.read(byteArray)
      byteArray
    } eventually {
      finOpt.foreach(_.close())
    }
  }
}
