package helpers

import db.DBSession
import schema.{Calca, Exercise}
import org.squeryl.PrimitiveTypeMode._

import scala.collection.mutable.ListBuffer

/**
  * Created by Valentin on 14/02/2018.
  */
object ExerciseCache extends DBSession {
  protected var exercises: ListBuffer[Exercise] = ListBuffer.empty
  protected val syncTime : Long = 60 // seconds

  def start() {
    sync()
    val t = new java.util.Timer()
    val task = new java.util.TimerTask { def run() = sync() }
    t.schedule(task, syncTime * 1000, syncTime * 1000L)
  }

  protected def sync() {
    transaction {
      exercises = Calca.exercises.to[ListBuffer]
    }
  }

  def search(string: String) : ListBuffer[Exercise] = {
    exercises.filter(m => m.name.toLowerCase.contains(string.toLowerCase))
  }
}