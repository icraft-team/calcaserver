package schema

import db.Time
import model.AchievementTitle
import org.squeryl.KeyedEntity

/**
  * Created by Valentin on 21/03/2018.
  */
class Reward(val goldAmount: Int, val achievementTitle: String) extends KeyedEntity[Long] {
  val id: Long = 0
  val dateClaimed: String = Time.stringFromEpoch(0)
  val timeClaimed: Long = 0L
  val claimed: Boolean = false
  var userID: Long = 0L
}
