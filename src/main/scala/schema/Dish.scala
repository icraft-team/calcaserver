package schema

import org.squeryl.KeyedEntity

/**
  * Created by Valentin on 01/05/2017.
  */
class Dish(var name : String, var calsPer : Int, var singled : Boolean) extends KeyedEntity[Long] {
  var id : Long = 0L
  var pictureURL : String = ""
}
