package schema

import org.squeryl.KeyedEntity
import org.squeryl.annotations.Transient

/**
  * Created by Valentin on 15/01/2018.
  */
class Exercise extends KeyedEntity[Long] {
  var id : Long = 0L
  var name : String = ""
  var MET : Double = 0
  var tags : String = ""

  @Transient def caloriesBurnt(weightInKilos: Float, minutes: Int) : Int = {
    val hours = minutes.toFloat/60
    val caloriesBurnt = MET * hours * weightInKilos
    caloriesBurnt.toInt
  }
}
