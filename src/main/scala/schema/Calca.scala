package schema

import org.squeryl.PrimitiveTypeMode._
import org.squeryl.Schema

/**
  * Created by Valentin on 26/01/17.
  */
object Calca extends Schema {
  val users = table[User]
  val dishes = table[Dish]
  val meals = table[Meal]
  val exercises = table[Exercise]
  val performedExercises = table[PerformedExercise]
  val rewards = table[Reward]

  on(users)(u => declare(
    u.id is (unique,indexed,autoIncremented,primaryKey),
    u.login is indexed
  ))

  on(dishes)(d => declare(
    d.id is (unique,indexed,autoIncremented,primaryKey),
    d.name is indexed,
    d.calsPer is indexed
  ))

  on(meals)(m => declare(
    m.id is (unique,indexed,autoIncremented,primaryKey),
    m.totalCals is indexed,
    m.dishName is indexed,
    m.time is indexed,
    m.userID is indexed
  ))

  on(exercises)(e => declare(
    e.id is (unique,indexed,autoIncremented,primaryKey),
    e.name is indexed
  ))

  on(performedExercises)(pe => declare(
    pe.id is (unique,indexed,autoIncremented,primaryKey),
    pe.userID is indexed,
    pe.exerciseID is indexed,
    pe.time is indexed
  ))

  on(rewards)(r => declare(
    r.id is (unique,indexed,autoIncremented,primaryKey),
    r.userID is indexed
  ))
}
