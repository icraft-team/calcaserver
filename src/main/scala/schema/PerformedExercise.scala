package schema

import db.Time
import org.squeryl.KeyedEntity

/**
  * Created by Valentin on 02/03/2018.
  */
class PerformedExercise() extends KeyedEntity[Long] {
  var id : Long = 0L
  var exerciseID : Long = 0L
  var name : String = ""
  var caloriesBurnt : Int = 0
  var userID : Long = 0L
  var time : Long = Time.now.getTime
  var date: String = Time.nowString
}
