package schema

import java.beans.Transient

import db.Time
import org.squeryl.KeyedEntity

/**
  * Created by Valentin on 01/05/2017.
  */
class Meal(@Transient dish : Dish) extends KeyedEntity[Long] {
  var id : Long = 0L
  var totalCals : Int = 0
  var numberOfGramsOrServings : Int = 0
  var dishID : Long = 0L
  var dishName : String = ""
  var time : Long = Time.now.getTime
  var userID : Long = 0L
  var singled : Boolean = false
  var date: String = Time.nowString

//  configure()

  def configure() {
    dishID = dish.id
    dishName = dish.name
    singled = dish.singled
  }
}
