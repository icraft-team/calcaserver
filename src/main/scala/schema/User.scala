package schema

import db.Time
import org.squeryl.KeyedEntity
import org.squeryl.annotations.Transient


/**
  * Created by Valentin on 26/01/17.
  */

case class User(var login : String, var passHash : String = "", var email : String) extends KeyedEntity[Long] {
  var id : Long = 0L
  var phone : String = ""
  var caloriesPerDay : Int = 2000
  var registrationDate : Long = Time.now.getTime
  var timezoneOffset : Long = 0L // in seconds
  var weight : Float = 0
  var metricSystem : Boolean = false

  def weightInKilos : Float = {
    if (metricSystem) {
      weight
    } else {
      weight/2.2f
    }
  }
}
