package model

import helpers.auth.SimpleAuthManager
import schema.{Calca, Reward, User}
import org.squeryl.PrimitiveTypeMode._

import scala.collection.mutable.ListBuffer
/**
  * Created by Valentin on 02/03/17.
  */

class Profile (var user: User, var authToken: String) {
  var rewards: ListBuffer[Reward] = ListBuffer.empty
}

object Profile {
  def getWithToken(authToken : String): Profile = {
    val userID = SimpleAuthManager.getUserIdByToken(authToken)
    Calca.users.where(u => u.id === userID).headOption match {
      case Some(u) => new Profile(u, authToken)
      case None => {
        print("Profile with this auth token not found: " + authToken)
        null
      }
    }
  }
}
