package model.achievements

import handlers.UserData
import model.{Achievement, AchievementTitle}
import model.achievements.traits.Backend
import schema.{Meal, PerformedExercise, User}

/**
  * Created by Valentin on 22/03/2018.
  */
class BurnCalories10000Achievement extends Achievement with Backend {
  val total: Int = 10000
  val name: String = "Burn total 10000 calories"
  val description: String = "Perform exercises everyday to burn calories"
  val goldReward: Int = 500
  val image: String = "fire10000.png"
  val daily = false
  val title = AchievementTitle.burnCalories10000.toString

  def calculateProgress(user: User, meals: Array[Meal], exercises: Array[PerformedExercise]) {
    val exerciseTodaySum: Int = exercises.foldLeft(0)(_ + _.caloriesBurnt)
    if (exerciseTodaySum > total) {
      progress = 1
    } else {
      progress = exerciseTodaySum.toFloat / total.toFloat
    }
  }
}
