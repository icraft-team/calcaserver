package model.achievements.traits

import schema.{Meal, PerformedExercise, User}

/**
  * Created by Valentin on 23/03/2018.
  */
trait Local {
  val local: Boolean = true
  def calculateProgress(user: User, activities: (Array[Meal], Array[PerformedExercise])) = 0
}
