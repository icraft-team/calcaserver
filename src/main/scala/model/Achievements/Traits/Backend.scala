package model.achievements.traits

import schema.{Meal, PerformedExercise, User}
/**
  * Created by Valentin on 21/03/2018.
  */
trait Backend {
  val local: Boolean = false
}
