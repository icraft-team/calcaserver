package model.achievements

import db.Time
import handlers.UserData
import model.{Achievement, AchievementTitle}
import schema.{Meal, PerformedExercise, User}
/**
  * Created by Valentin on 21/03/2018.
  */
class BurnCaloriesAchievementDaily extends Achievement {
  val total: Int = 500
  val name: String = "Burn 500 calories"
  val description: String = "Perform exercises today to burn calories"
  val goldReward: Int = 100
  val image: String = "fire.png"
  val daily: Boolean = true
  val title = AchievementTitle.burnCaloriesDaily.toString
  val local = false

  def calculateProgress(user: User, meals: Array[Meal], exercises: Array[PerformedExercise]) {
    val startOfToday = Time.startOfToday + user.timezoneOffset
    val todayExercises = exercises.filter(pe => pe.time >= startOfToday)
    val exerciseTodaySum = todayExercises.foldLeft(0)(_ + _.caloriesBurnt)
    if (exerciseTodaySum > total) {
      progress = 1
    } else {
      progress = exerciseTodaySum.toFloat / total.toFloat
    }
  }
}
