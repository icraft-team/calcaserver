package model

import db.Time
import handlers.UserData
import schema._

object AchievementTitle extends Enumeration {
  val burnCaloriesDaily = Value("BurnCaloriesDaily")
  val burnCalories10000 = Value("BurnCalories10000")
}

abstract class Achievement {
  val name: String
  val description: String
  val goldReward: Int
  val image: String
  val total: Int
  val daily: Boolean
  val title: String
  val local: Boolean

  var rewardClaimed: Boolean = false
  var progress: Float = 0

  def calculateProgress(user: User, meals: Array[Meal], exercises: Array[PerformedExercise])

  def checkReward(user: User, userRewards: Array[Reward]) {
    if (progress < 1) return

    val checkDate = if (daily) Time.startOfToday + user.timezoneOffset else Long.MaxValue
    userRewards.find(r => checkDate >= r.timeClaimed) match {
      case Some(reward) => rewardClaimed = reward.claimed
      case None =>
        val reward = new Reward(goldReward, title.toString)
        reward.userID = user.id
        Calca.rewards.insert(reward)
    }
  }
}
