package com

import akka.actor.Actor
import handlers._
import helpers.auth.SimpleAuthManager
import spray.routing._
import model._
import spray.http.{HttpCookie, HttpResponse, StatusCodes}
import spray.httpx.encoding.Deflate

// we don't implement our route structure directly in the service actor because
// we want to be able to test it independently, without having to spin up an actor
class MyServiceActor extends Actor with MainService {
  // the HttpService trait defines only one abstract member, which
  // connects the services environment to the enclosing actor or test
  def actorRefFactory = context

  // this actor only runs our route, but you could add
  // other things here, like request stream processing
  // or timeout handling
  def receive = runRoute(mainRoute)
}

// this trait defines our service behavior independently from the service actor
trait MainService extends HttpService {
  val mainRoute = {
    get {
      path ("") { complete { "ops" } } ~
      path ("hey") { cookie("authToken") { token => complete { "hey" } } } ~
      path ("ping") { complete { new PingHandler().process } } ~
      path ("hello" / IntNumber ) { number => complete { new TestHandler(number).process } } ~
      path ("mealAutocomplete" / Rest) { searchString => complete(new DishAutocompleteHandler(searchString).process) } ~
      path ("exerciseAutocomplete" / Rest) { searchString =>
        cookie("authToken") { token =>
          complete(new ExerciseAutocompleteHandler(searchString).process)
        }
      } ~
      pathPrefix ("images") { getFromDirectory("images/") } ~
      pathPrefix ("achievements") {
        cookie("authToken") { token =>
          complete( auth(token, new AchievementsHandler(token.content)) )
        }
      }
    } ~
    post {
      path("auth") { entity(as[String]) { json => complete(new AuthHandler(json).process) } } ~
        path("restoreAuth") { entity(as[String]) { json =>
          complete(new AuthHandler(null).restoreAuth(json))
        }} ~
        path("registration") { entity(as[String]) { json =>
          complete(new RegistrationHandler(json).process) }
        } ~
        path("addMeal") {
          cookie("authToken") { token =>
            entity(as[String]) { json => complete( auth(token, new AddMealHandler(json, token.content))) }
          }
        } ~
        path("addPerformedExercise") {
          cookie("authToken") { token =>
            entity(as[String]) { json => complete(auth(token, new AddPerformedExerciseHandler(json, token.content))) }
          }
        } ~
        path("createDish") {
          cookie("authToken") { token =>
            entity(as[String]) { json => complete(auth(token, new CreateDishHandler(json, token.content))) }
          }
        } ~
        path("syncUser") {
          cookie("authToken") { token =>
            entity(as[String]) { json => complete(auth(token, new UserSyncHandler(json, token.content))) }
          }
        } ~
        path ("userActivities") {
          cookie("authToken") { token =>
            entity(as[String]) { json => complete(auth(token, new UserActivitiesHandler(json, token.content))) }
          }
        }
    } ~ delete {
      path("deleteMeal" / LongNumber) { mealID =>
        cookie("authToken") { token =>
          entity(as[String]) { json => complete(auth(token, new DeleteMealHandler(mealID, token.content))) }
        }
      }
    } ~ delete {
      path("deletePerformedExercise" / LongNumber) { exerciseID =>
        cookie("authToken") { token =>
          entity(as[String]) { json => complete(auth(token, new DeleteExerciseHandler(exerciseID, token.content))) }
        }
      }
    } ~
    path ("hey") { cookie("authToken") { token => complete { "hey" } } }
  }

  def auth(token: HttpCookie, handler: BaseHandler) : (HttpResponse) = {
    if (SimpleAuthManager.check(token.content)) handler.process
    else HttpResponse(StatusCodes.Unauthorized, "token expired")
  }
}